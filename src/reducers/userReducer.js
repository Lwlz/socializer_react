import { USER_ID, USER } from '../actions/types';

const initialState = {
  uid: '',
  username: '',
  userdata: {}
};

export default function(state = initialState, action) {
  console.log(action.type, action.payload);

  switch (action.type) {
    case USER_ID:
      return {
        ...state,
        uid: action.payload,
        userName: action.payload
      };
    case USER:
      return {
        ...state,
        [action.payload.prop]: action.payload.value
      };
    default:
      return state;
  }
}
