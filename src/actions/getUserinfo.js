import { USER } from '../actions/types';

export const userInfo = ({ prop, value }) => {
  return { type: USER, payload: { prop, value } };
};
