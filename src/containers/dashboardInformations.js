import React, { Component } from 'react';

// import { connect } from 'react-redux';
// import { userInfo } from '../actions/getUserinfo';

class dashboardInformations extends Component {
  state = {
    firstName: 'Anis',
    lastName: 'Bar',
    adresse: '11 rue du ...',
    phone: '06118218',
    email: 'example@mail.com'
  };

  render() {
    return (
      <div id='mainContent'>
        <div className='title-dashboard'>
          <h2>Mes informations</h2>
        </div>

        <div className='button-group'>
          <button
            type='button'
            className='btn btn-transparent btn-border--blue'
          >
            <i className='fas fa-edit' />
            modifier mes informations
          </button>
          <button type='button' className='btn btn-green'>
            <i className='fas fa-camera-retro' />
            modifier ma photo
          </button>
        </div>

        <div className='user-info-wrapper'>
          <div className='white-card mh'>
            <div className='info-user-line'>
              <label>Nom:</label>
              <span>{localStorage.getItem('nom')}</span>
            </div>
            <div className='info-user-line'>
              <label>Prénom:</label>
              <span>{localStorage.getItem('prenom')}</span>
            </div>

            {/* <div className='info-user-line'>
              <label>Adresse:</label>
              <span>{localStorage.getItem('prenom')}</span>
            </div> */}
            {/* <div className='info-user-line'>
              <label>Téléphone:</label>
              <span>{localStorage.getItem('prenom')}</span>
            </div>
            <div className='info-user-line'>
              <label>Mail:</label>
              <span>{localStorage.getItem('prenom')}</span>
            </div> */}
          </div>
          <div className='white-card mh'>
            <div className='info-user-line'>
              <label>Adresse:</label>
              <span>in the aws cloud</span>
            </div>
            <div className='info-user-line'>
              <label>Téléphone:</label>
              <span>118 218</span>
            </div>
            <div className='info-user-line'>
              <label>Mail:</label>
              <span>{localStorage.getItem('email')}</span>
            </div>
          </div>
        </div>

        <div className='py-5'>
          <button
            type='button'
            className='btn btn-squared btn-blue'
            //   data-toggle='modal'
            //   data-target='#modalPhoto'
          >
            <i className='fas fa-key' />
            modifier mon mot de passe
          </button>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   Userdata: state.userRed.userdata
// });

// export default connect(
//   mapStateToProps,
//   { userInfo }
// )(dashboardInformations);

export default dashboardInformations;
