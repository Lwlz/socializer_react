import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as actions from '../store/actions/index';
import { updateObject, checkValidity } from '../shared/utility';

class signUp extends Component {
  state = {
    email: '',
    password: '',
    confirmPassword: '',
    lastName: '',
    firstName: '',
    username: '',
    isSignup: true
  };

  componentDidUpdate() {
    // console.log('the state', this.state);
    // console.log('the props', this.props);
    // console.log('Authenticated', this.props.isAuthenticated);
    // if (this.props.isAuthenticated) {
    //   // this.props.onSetAuthRedirectPath();
    // }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  submitHandler = event => {
    const {
      email,
      password,
      confirmPassword,
      lastName,
      firstName,
      username
    } = this.state;
    event.preventDefault();

    const authData = {
      email: email,
      password: password,
      username: username,
      confirmPassword,
      lastName,
      firstName
    };

    let url = 'https://back-socializer.herokuapp.com/auth/signup';
    axios
      .post(url, authData)
      .then(response => {
        console.log('the reponse signuppp', response);
        const expirationDate = new Date(
          new Date().getTime() + response.data.expiresIn * 1000
        );
        //need to work the back corrections
        localStorage.setItem('token', response.data.JWT);
        localStorage.setItem('expirationDate', expirationDate);
        localStorage.setItem('userId', response.data._id);
        localStorage.setItem('nom', response.data.firstName);
        localStorage.setItem('prenom', response.data.lastName);
        localStorage.setItem('email', response.data.email);
        this.props.settingCo(response.data.idToken, response.data._id);
        this.props.history.push('/dashboard/monit');
      })
      .catch(err => {
        console.log('[ the errors : ]', err);
      });
  };

  render() {
    return (
      <div className='register-user-section container'>
        <div className='main-title'>
          <h1>
            Inscrivez-vous sur
            <span className='color-red' />
            Socializer.
          </h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>

        {this.props.loading ? null : (
          <form className='register-user-form'>
            <div className='row'>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Prénom</label>
                  <input
                    name='firstName'
                    type='text'
                    placeholder='Prénom'
                    className='form-control'
                    onChange={this.onChange}
                    value={this.state.firstName}
                  />
                </div>
              </div>

              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Nom</label>
                  <input
                    name='lastName'
                    type='text'
                    placeholder='Nom'
                    className='form-control'
                    onChange={this.onChange}
                    value={this.state.lastName}
                  />
                </div>
              </div>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Nom d'utilisateur</label>
                  <input
                    name='username'
                    type='text'
                    maxLength='180'
                    pattern='.{5,}'
                    placeholder="Nom d'utilisateur"
                    className='form-control'
                    title='Au moins 5 caractéres'
                    onChange={this.onChange}
                    value={this.state.username}
                  />
                </div>
              </div>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Email</label>
                  <input
                    name='email'
                    type='email'
                    placeholder='Email'
                    className='form-control'
                    onChange={this.onChange}
                    value={this.state.email}
                  />
                </div>
              </div>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Mot de passe</label>
                  <input
                    name='password'
                    type='password'
                    placeholder='Mot de passe'
                    className='form-control'
                    onChange={this.onChange}
                    value={this.state.password}
                  />
                  <p className='help-block'>
                    Au moins 6 caractéres, avec une majuscule, une minuscule et
                    un chiffre
                  </p>
                </div>
              </div>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Confirmation mot de passe</label>
                  <input
                    name='confirmPassword'
                    type='password'
                    placeholder='Confirmez votre mot de passe'
                    className='form-control'
                    onChange={this.onChange}
                    value={this.state.confirmPassword}
                  />
                </div>
              </div>
              <div className='col-md-12 col-xs-12'>
                <div className='cgu-wrapper'>
                  <input
                    style={{ marginRight: 8 }}
                    type='checkbox'
                    id='cgu'
                    name='cgu'
                  />
                  En vous inscrivant en tant qu'utilisateur, vous confirmez que
                  vous acceptez les Conditions Générales du site et déclarez
                  être informé des obligations civiles et fiscales qui incombent
                  aux photographes exerçant une activité rémunérée.
                  <a
                    target='_blank'
                    href='/cgu'
                    style={{ marginLeft: 4, color: 'blue' }}
                  >
                    Lire les CGU
                  </a>
                </div>
              </div>
            </div>
            <button
              // onClick={this.createUser}
              onClick={this.submitHandler}
              style={{
                marginTop: 8
              }}
              className='btn btn-rounded btn-green'
            >
              Je m'inscris
            </button>
          </form>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuthSign: (email, password, confirmPassword, lastName, firstName) =>
      dispatch(
        actions.authSign(email, password, confirmPassword, lastName, firstName)
      ),
    settingCo: (idToken, id) => dispatch(actions.authSuccess(idToken, id)),

    onSetAuthRedirectPath: () =>
      dispatch(actions.setAuthRedirectPath('/pricing'))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(signUp);
