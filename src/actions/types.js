export const FETCH_ERROR = 'FETCH_ERROR';
export const FETCH_PROCESS = 'FETCH_PROCESS';
export const FETCH_SUCESS = 'FETCH_SUCESS';

export const ADD_SUCESS = 'ADD_SUCESS';
export const ADD_ERROR = 'ADD_ERROR';
export const ADDING_POKE = 'ADDING_POKE';

export const FETCH_POSTS = 'FETCH_POSTS';
export const NEW_POST = 'NEW_POST';

export const DELIRE = 'DELIRE';

export const USER = 'USER';
export const USER_ID = 'USER_ID';
