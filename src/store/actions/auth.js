import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (token, userId) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    idToken: token,
    userId: userId
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('expirationDate');
  localStorage.removeItem('userId');
  localStorage.removeItem('nom');
  localStorage.removeItem('prenom');
  localStorage.removeItem('email');
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const authSign = (
  email,
  password,
  confirmPassword,
  lastName,
  firstName
) => {
  return dispatch => {
    dispatch(authStart());
    const authData = {
      email: email,
      password: password,
      confirmPassword,
      lastName,
      firstName
      // returnSecureToken: true
    };
    let url = 'https://back-socializer.herokuapp.com/auth/signup';
    axios
      .post(url, authData)
      .then(response => {
        console.log('the reponse signup from redux', response);
        const expirationDate = new Date(
          new Date().getTime() + response.data.expiresIn * 1000
        );
        localStorage.setItem('token', response.data.JWT);
        localStorage.setItem('expirationDate', expirationDate);
        localStorage.setItem('userId', response.data.id);
        localStorage.setItem('nom', response.data.nom);
        localStorage.setItem('prenom', response.data.prenom);
        localStorage.setItem('email', response.data.email);
        dispatch(authSuccess(response.data.idToken, response.data.id));
        dispatch(checkAuthTimeout(response.data.expiresIn));
      })
      .catch(err => {
        dispatch(authFail(err));
      });
  };
};

export const auth = (email, password, isSignup) => {
  return dispatch => {
    dispatch(authStart());
    const authData = {
      email: email,
      password: password,
      returnSecureToken: true
    };
    // let url = 'http://localhost:4300/auth/signup';
    let url = 'https://back-socializer.herokuapp.com/auth/signup';
    if (!isSignup) {
      // url = 'http://localhost:4300/auth/signin';
      url = 'https://back-socializer.herokuapp.com/auth/signin';
    }
    axios
      .post(url, authData)
      .then(response => {
        console.log('the reponse auth from redux', response);
        const expirationDate = new Date(
          new Date().getTime() + response.data.expiresIn * 1000
        );
        localStorage.setItem('token', response.data.JWT);
        localStorage.setItem('expirationDate', expirationDate);
        localStorage.setItem('userId', response.data.id);
        localStorage.setItem('nom', response.data.nom);
        localStorage.setItem('prenom', response.data.prenom);
        localStorage.setItem('email', response.data.email);
        dispatch(authSuccess(response.data.idToken, response.data.id));
        dispatch(checkAuthTimeout(response.data.expiresIn));
      })
      .catch(err => {
        console.log('the error is ', err);
        dispatch(authFail(err));
      });
  };
};

export const setAuthRedirectPath = path => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path: path
  };
};

export const authCheckState = () => {
  return dispatch => {
    const token = localStorage.getItem('token');
    if (!token) {
      dispatch(logout());
    } else {
      const expirationDate = new Date(localStorage.getItem('expirationDate'));
      if (expirationDate <= new Date()) {
        dispatch(logout());
      } else {
        const userId = localStorage.getItem('userId');
        dispatch(authSuccess(token, userId));
        dispatch(
          checkAuthTimeout(
            (expirationDate.getTime() - new Date().getTime()) / 1000
          )
        );
      }
    }
  };
};
