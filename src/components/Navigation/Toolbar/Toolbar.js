import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

const toolbar = props => (
  <header className='header'>
    <DrawerToggle clicked={props.drawerToggleClicked} />
    <div className='header-logo'>
      <Logo />
    </div>
    <nav className='menu-wrapper DesktopOnly'>
      <NavigationItems isAuthenticated={props.isAuth} />

      <div className='social-wrapper'>
        <ul>
          <li>
            <a
              href='//facebook.com'
              target='_blank'
              rel='noopener noreferrer'
              className='nav-item'
            >
              <i className='fab fa-facebook-f' />
            </a>
            <a
              href='//instagram.com/lwlz.inc/'
              target='_blank'
              rel='noopener noreferrer'
              className='nav-item'
            >
              <i className='fab fa-instagram' />
            </a>
            <a
              href='//linkedin.com/in/anis-barka-152083152/'
              target='_blank'
              rel='noopener noreferrer'
              className='nav-item'
            >
              <i className='fab fa-linkedin-in' />
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
);

export default toolbar;
