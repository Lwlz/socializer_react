import React, { Component } from 'react';
import { NavLink as Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userInfo } from '../actions/getUserinfo';

class Sidebar extends Component {
  render() {
    return (
      <div>
        <aside id='sideBarUser'>
          <div className='header-user'>
            <div className='logo-wrapper' />
            <div className='profil-picture no-picture'>
              <a href='/'>
                <div className='no-picture-wrapper'>
                  <i className='fas fa-upload' />
                  <p>Ajouter une photo</p>
                </div>
              </a>
            </div>
            <h3
              style={{
                // marginLeft: 20,
                marginTop: 8,
                textAlign: 'center'
              }}
            >
              {localStorage.getItem('prenom')}
            </h3>
          </div>
          {/* menu */}
          <nav
            style={{
              marginLeft: 20
            }}
          >
            <ul>
              <li>
                <Link to='/dashboard/monit' activeClassName='active'>
                  <i className='fas fa-camera-retro' />
                  monitoring
                </Link>
              </li>
              <li>
                <Link to='/dashboard/message' activeClassName='active'>
                  <i className='far fa-comments' />
                  messagerie
                </Link>
              </li>
              <li>
                <Link to='/dashboard/info' activeClassName='active'>
                  <i className='fas fa-user-circle' />
                  informations
                </Link>
              </li>
              <li>
                <Link to='/dashboard/pref' activeClassName='active'>
                  <i className='fas fa-cog' />
                  préférences
                </Link>
              </li>
            </ul>
          </nav>

          <div className='footer-user'>
            <Link to='/logout' className='btn btn-reset'>
              <i className='fas fa-power-off' />
              déconnexion
            </Link>
          </div>
        </aside>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  // Userdata: state.userRed.userdata
});
export default connect(
  mapStateToProps,
  { userInfo }
)(Sidebar);
