import React, { Component } from 'react';
import Sidebar from '../components/Sidebar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import dashboardInformations from './dashboardInformations';
import dashboardMessagerie from './/dashboardMessagerie';
import dashboardMonitoring from './dashboardMonitoring';
import dashboardPreferences from './dashboardPreferences';

export default class dashboard extends Component {
  render() {
    return (
      <div>
        <div className='dashboard-container'>
          <Sidebar />
          <Route
            exact
            path='/dashboard/monit'
            component={dashboardMonitoring}
          />
          <Route
            exact
            path='/dashboard/message'
            component={dashboardMessagerie}
          />
          <Route
            exact
            path='/dashboard/info'
            component={dashboardInformations}
          />
          <Route
            exact
            path='/dashboard/pref'
            component={dashboardPreferences}
          />
        </div>
      </div>
    );
  }
}
